package az.ingress.usersecurity.dto;

import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SignUpRequest {

    private String name;

    private String username;

    private String email;

    @Size(min = 4, max = 10)
    private String password;


}