package az.ingress.usersecurity.exception;

import lombok.Getter;

@Getter
public class NotFoundException extends RuntimeException {

    public final ErrorCodes errorCode;

    public NotFoundException(ErrorCodes errorCode) {
        this.errorCode = errorCode;
    }
}
