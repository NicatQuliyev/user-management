package az.ingress.usersecurity.exception;

public enum ErrorCodes {
    USER_ALREADY_EXCEPTION,
    EMAIL_ALREADY_EXCEPTION,
    AUTHORITY_NOT_FOUND
}
