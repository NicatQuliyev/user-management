package az.ingress.usersecurity.exception;

import lombok.Getter;

@Getter
public class BadRequestException extends RuntimeException {

    public final ErrorCodes errorCode;
    public final transient Object[] arguments;

    public BadRequestException(ErrorCodes errorCode, Object... args) {
        this.errorCode = errorCode;
        this.arguments = args == null ? new Object[0] : args;
    }
}
