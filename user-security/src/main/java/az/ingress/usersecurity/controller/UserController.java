package az.ingress.usersecurity.controller;

import az.ingress.usersecurity.dto.SignUpRequest;
import az.ingress.usersecurity.dto.SignUpResponse;
import az.ingress.usersecurity.service.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/auth")
@RequiredArgsConstructor
@Slf4j
public class UserController {

    private final UserService userService;

    @PostMapping("/signup")
    public ResponseEntity<SignUpResponse> registerUser(@RequestBody @Valid SignUpRequest signUpRequest) {

        return userService.registerUser(signUpRequest);

    }

    @GetMapping("/confirmation")
    public ResponseEntity<?> confirmation(@RequestParam("confirmationToken") String confirmationToken) {
        return userService.confirmation(confirmationToken);
    }

    @GetMapping("/1")
    public ResponseEntity<String> hello1() {
        log.info("Hello1 method is work");
        //go to service
        log.info("Hello1 method is done");
        return ResponseEntity.ok("Hello from another ms ");
    }

    @GetMapping("/2")
    public ResponseEntity<String> hello2() {
        log.info("Hello2 method is work");
        //go to service
        log.info("Hello2 method is done");
        return ResponseEntity.ok("Hello from another ms ");
    }

    @GetMapping("/3")
    public ResponseEntity<String> hello3() {
        log.info("Hello3 method is work");
        //go to service
        log.info("Hello3 method is done");
        return ResponseEntity.ok("Hello from another ms ");
    }
}
