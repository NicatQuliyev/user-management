package az.ingress.usersecurity.service;

import az.ingress.usersecurity.dto.SignUpRequest;
import az.ingress.usersecurity.dto.SignUpResponse;
import az.ingress.usersecurity.exception.BadRequestException;
import az.ingress.usersecurity.exception.ErrorCodes;
import az.ingress.usersecurity.exception.NotFoundException;
import az.ingress.usersecurity.model.Authority;
import az.ingress.usersecurity.model.User;
import az.ingress.usersecurity.repository.AuthorityRepository;
import az.ingress.usersecurity.repository.UserRepository;
import az.ingress.usersecurity.security.JwtService;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RequiredArgsConstructor
@Service
public class UserService {

    private final UserRepository userRepository;

    private final EmailService emailService;

    private final AuthorityRepository authorityRepository;

    private final BCryptPasswordEncoder passwordEncoder;

    private final JwtService jwtService;

    private final KafkaTemplate<String, Object> kafkaTemplate;

    public User getById(Long id) {
        return userRepository.findById(id).orElseThrow();
    }

    public ResponseEntity<SignUpResponse> registerUser(SignUpRequest signUpRequest) {

        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            throw new BadRequestException(ErrorCodes.USER_ALREADY_EXCEPTION, signUpRequest.getUsername());
        }

        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            throw new BadRequestException(ErrorCodes.EMAIL_ALREADY_EXCEPTION, signUpRequest.getEmail());
        }

        Authority userAuthority = authorityRepository.findByAuthority("USER")
                .orElseThrow(() -> new NotFoundException(ErrorCodes.AUTHORITY_NOT_FOUND));


        // Creating user's account
        User user = User.builder()
                .authorities(List.of(userAuthority))
                .name(signUpRequest.getName())
                .password(signUpRequest.getPassword())
                .username(signUpRequest.getUsername())
                .email(signUpRequest.getEmail())
                .build();

        user.setPassword(passwordEncoder.encode(signUpRequest.getPassword()));

        String confirmationToken = getConfirmationToken();

        user.setConfirmationToken(confirmationToken);
        userRepository.save(user);

        List<Header> headers = new ArrayList<>();
        headers.add(new RecordHeader("Accept-Language", "az".getBytes()));
        kafkaTemplate.send(new ProducerRecord<>("user-register", 0, "1", signUpRequest));

        emailService.sendMail(signUpRequest.getEmail(),
                "Confirmation",
                "http://localhost:9090/api/auth/confirmation?confirmationToken=" + confirmationToken);


        return ResponseEntity.ok(SignUpResponse
                .builder()
                .jwt(jwtService.issueToken(user))
                .build());

    }

    public ResponseEntity<?> confirmation(String confirmationToken) {
        Optional<User> user = userRepository.findByConfirmationToken(confirmationToken);
        if (user.isPresent()) {
            User user1 = user.get();
            userRepository.save(user1);

            return ResponseEntity.ok("User confirmed successfully");
        } else {
            return ResponseEntity.ok("Confirmation token is invalid");
        }


    }

    private String getConfirmationToken() {
        UUID gfg = UUID.randomUUID();
        return gfg.toString();
    }
}
