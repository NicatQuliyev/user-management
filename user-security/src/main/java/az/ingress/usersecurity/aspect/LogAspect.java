package az.ingress.usersecurity.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Slf4j
public class LogAspect {
    @Pointcut("execution(public * az.ingress.usersecurity.service.UserService.*(..))")
    public void callAtMyService() {
    }

    @Before("callAtMyService()")
    public void beforeCallAtMethod(JoinPoint jp) {

        log.info("ASPECT: {} method is working ", jp.getSignature().getName());
    }

    @AfterReturning(value = "callAtMyService()", returning = "obj")
    public void afterCallAt(JoinPoint jp, Object obj) {

        log.info("ASPECT: {} method is done returning value {}", jp.getSignature().getName(), obj);
    }
}
