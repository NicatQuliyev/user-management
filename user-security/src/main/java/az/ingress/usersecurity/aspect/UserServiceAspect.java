//package az.ingress.usersecurity.aspect;
//
//import az.ingress.usersecurity.dto.SignUpRequest;
//import az.ingress.usersecurity.dto.SignUpResponse;
//import az.ingress.usersecurity.model.User;
//import lombok.RequiredArgsConstructor;
//import org.aspectj.lang.ProceedingJoinPoint;
//import org.aspectj.lang.annotation.Around;
//import org.aspectj.lang.annotation.Aspect;
//import org.springframework.cache.Cache;
//import org.springframework.cache.CacheManager;
//import org.springframework.stereotype.Component;
//
//@Aspect
//@Component
//@RequiredArgsConstructor
//public class UserServiceAspect {
//
//    private final CacheManager cacheManager;
//
//    @Around("execution(* az.ingress.usersecurity.service.UserService.getById(..))" +
//            "&& args(id,..)")
//    public User getFromCacheOrExecute(ProceedingJoinPoint proceedingJoinPoint, Long id) throws Throwable {
//        var cache = cacheManager.getCache("register");
//        var user = cache.get(id, User.class);
//
//        if (user == null) {
//            var proceed = (User) proceedingJoinPoint.proceed();
//            cache.put(id, proceed);
//
//            return proceed;
//        }
//
//        return user;
//    }
//}
