package az.ingress.usersecurity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.EnableKafka;

@SpringBootApplication
@EnableKafka
public class UserSecurityApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserSecurityApplication.class, args);
    }

}
