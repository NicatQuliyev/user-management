package az.ingress.userservice.exception;

import lombok.Getter;

@Getter
public class UserExistException extends RuntimeException {

    public final ErrorCodes errorCode;

    public UserExistException(ErrorCodes errorCode) {
        this.errorCode = errorCode;
    }
}
