package az.ingress.userservice.exception;

public enum ErrorCodes {
    USER_NOT_FOUND,
    USER_EXIST
}
