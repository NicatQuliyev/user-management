package az.ingress.userservice.exception;

import az.ingress.userservice.response.UserResponse;
import az.ingress.userservice.service.impl.TranslationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.util.Map;

import static org.springframework.http.HttpHeaders.ACCEPT_LANGUAGE;

@ControllerAdvice
@RequiredArgsConstructor
public class GlobalExceptionHandler {

    private final TranslationService translationService;

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ErrorResponseDto> handleNotFoundException(NotFoundException ex,
                                                                    WebRequest req) {


        var lang = req.getHeader(ACCEPT_LANGUAGE) == null ? "en" : req.getHeader(ACCEPT_LANGUAGE);
        ex.printStackTrace();

        return ResponseEntity.status(404).body(ErrorResponseDto.builder()
                .status(404)
                .title("Exception")
                .details(translationService.findByKey(ex.getErrorCode().name(), lang))
                .build());
    }

    @ExceptionHandler(UserExistException.class)
    public ResponseEntity<ErrorResponseDto> handleUserExistException(UserExistException ex,
                                                                     WebRequest req) {
        ErrorResponseDto responseDto = ErrorResponseDto.builder()
                .status(403)
                .title("Exception")
                .details("User already exist")
                .build();

        ex.printStackTrace();

        return ResponseEntity.status(403).body(responseDto);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorResponseDto> handleMethodArgumentNotValidException(MethodArgumentNotValidException ex,
                                                                                  WebRequest req) {
        ex.printStackTrace();
        ErrorResponseDto response = ErrorResponseDto.builder()
                .status(400)
                .title("Exception")
                .details("Validation Error")
                .build();

        ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .forEach(error -> {
                    Map<String, String> data = response.getData();
                    data.put(error.getField(), error.getDefaultMessage());
                });
        return ResponseEntity.status(400).body(response);
    }
}
