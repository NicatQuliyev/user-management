package az.ingress.userservice.aspect;

import az.ingress.userservice.exception.ErrorCodes;
import az.ingress.userservice.exception.UserExistException;
import az.ingress.userservice.repository.UserRepository;
import az.ingress.userservice.request.UserRequest;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.ProceedingJoinPoint;

import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
@RequiredArgsConstructor
public class UserServiceAspect {

    private final UserRepository userRepository;

    @Around("@annotation(az.ingress.userservice.aspect.UserExist) && args(userRequest)")
    public Object checkUserExistence(ProceedingJoinPoint joinPoint, UserRequest userRequest) throws Throwable {
        if (userRepository.existsUserByEmail(userRequest.getEmail())) {
            throw new UserExistException(ErrorCodes.USER_EXIST);
        } else {
            return joinPoint.proceed();
        }
    }
}
