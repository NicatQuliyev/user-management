package az.ingress.userservice.response;

import lombok.Builder;
import lombok.Data;

@Data
public class UserResponse {
    Long id;

    String name;

    String surname;

    String email;

    String phoneNumber;
}
