package az.ingress.userservice.service;


import az.ingress.userservice.request.UserRequest;
import az.ingress.userservice.response.UserResponse;

import java.util.List;

public interface UserService {

    List<UserResponse> findAll();

    UserResponse findById(Long userId);

    UserResponse save(UserRequest userRequest);

    UserResponse update(UserRequest userRequest, Long userId);

    void delete(Long userId);

}
