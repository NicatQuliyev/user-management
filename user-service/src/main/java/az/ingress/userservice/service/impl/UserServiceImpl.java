package az.ingress.userservice.service.impl;

import az.ingress.userservice.aspect.UserExist;
import az.ingress.userservice.exception.ErrorCodes;
import az.ingress.userservice.exception.NotFoundException;
import az.ingress.userservice.mapper.UserMapper;
import az.ingress.userservice.model.User;
import az.ingress.userservice.repository.UserRepository;
import az.ingress.userservice.request.UserRequest;
import az.ingress.userservice.response.UserResponse;
import az.ingress.userservice.service.UserService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final ModelMapper modelMapper;

    private final UserMapper userMapper;

    @Override
    public List<UserResponse> findAll() {
        return userRepository
                .findAll()
                .stream()
                .map(userMapper::entityToResponse)
                .collect(Collectors.toList());
    }

    @Override
    public UserResponse findById(Long userId) {
        User user = userRepository.findById(userId).orElseThrow(() -> new NotFoundException(ErrorCodes.USER_NOT_FOUND));

        return userMapper.entityToResponse(user);
    }

    @Override
    @UserExist
    public UserResponse save(UserRequest userRequest) {
        User user = modelMapper.map(userRequest, User.class);

        return userMapper.entityToResponse(userRepository.save(user));
    }

    @Override
    public UserResponse update(UserRequest userRequest, Long userId) {
        userRepository.findById(userId).orElseThrow(() -> new NotFoundException(ErrorCodes.USER_NOT_FOUND));
        User responseUser = userMapper.requestToEntity(userRequest);
        responseUser.setId(userId);

        return userMapper.entityToResponse(userRepository.save(responseUser));

    }

    @Override
    public void delete(Long userId) {
        User user = userRepository.findById(userId).orElseThrow(() -> new NotFoundException(ErrorCodes.USER_NOT_FOUND));

        userRepository.delete(user);
    }
}
