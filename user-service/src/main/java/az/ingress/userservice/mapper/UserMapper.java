package az.ingress.userservice.mapper;

import az.ingress.userservice.model.User;
import az.ingress.userservice.request.UserRequest;
import az.ingress.userservice.response.UserResponse;
import org.mapstruct.Mapper;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE)
public interface UserMapper {
    UserResponse entityToResponse(User user);

    User requestToEntity(UserRequest userRequest);

}
