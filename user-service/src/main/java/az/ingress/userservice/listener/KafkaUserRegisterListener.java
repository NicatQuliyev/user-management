package az.ingress.userservice.listener;

import az.ingress.userservice.request.SignUpRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class KafkaUserRegisterListener {

    @KafkaListener(topics = {"user-register"}, containerFactory = "kafkaJsonListenerContainerFactory")
    public void listen(ConsumerRecord<String, SignUpRequest> record) {
        log.info("Message received: {}", record);
    }
}
