package az.ingress.userservice.request;

import lombok.Data;

@Data
public class UserRequest {
    String name;

    String surname;

    String email;

    String phoneNumber;
}
