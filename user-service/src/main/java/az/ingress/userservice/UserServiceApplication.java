package az.ingress.userservice;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@RequiredArgsConstructor
@Slf4j
@EnableFeignClients
public class UserServiceApplication implements CommandLineRunner {

//    private final RestTemplate restTemplate = new RestTemplate();



    public static void main(String[] args) {
        SpringApplication.run(UserServiceApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
//        ResponseEntity<String> response = restTemplate.getForEntity("http://localhost:9090/api/auth/hello", String.class);
//
//        System.out.println(response.getBody());

//        ResponseEntity<String> hello = anotherMsClient.getHello();
//        System.out.println(hello.getBody());
    }
}
